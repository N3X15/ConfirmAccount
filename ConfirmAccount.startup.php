<?php
/*
 (c) Aaron Schulz 2007, GPL-2.0-or-later

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 https://www.gnu.org/copyleft/gpl.html
*/

if ( !defined( 'MEDIAWIKI' ) ) {
	echo "ConfirmAccount extension\n";
	exit( 1 );
}


/**
 * This function is for setup that has to happen in Setup.php
 * when the functions in $wgExtensionFunctions get executed.
 * @return void
 */
class ConfirmAccountStartup {
	static function efLoadConfirmAccount() {
		global $wgEnableEmail, $wgConfirmAccountFSRepos, $wgUploadDirectory;
		# This extension needs email enabled!
		# Otherwise users can't get their passwords...
		if ( !$wgEnableEmail ) {
			echo "ConfirmAccount extension requires \$wgEnableEmail set to true.\n";
			exit( 1 );
		}

		if ( $wgConfirmAccountFSRepos['accountreqs']['directory'] === false ) {
			$wgConfirmAccountFSRepos['accountreqs']['directory'] =
				$wgUploadDirectory . "/accountreqs";
		}
		if ( $wgConfirmAccountFSRepos['accountcreds']['directory'] === false ) {
			$wgConfirmAccountFSRepos['accountcreds']['directory'] =
				$wgUploadDirectory . "/accountcreds";
		}
	}
}
