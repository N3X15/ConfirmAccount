import os
from buildtools import os_utils
from buildtools.maestro import BuildMaestro
from buildtools.maestro.convert_data import EDataType, ConvertDataBuildTarget
m = BuildMaestro()
m.add(ConvertDataBuildTarget('extension.json', 'extension.yml', from_type=EDataType.YAML, to_type=EDataType.JSON, pretty_print=True, indent_chars='  '))
m.as_app()
