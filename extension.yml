manifest_version: 2
name: ConfirmAccount
namemsg: confirmaccount-extensionname
descriptionmsg: confirmaccount-desc
version: 1.1.0
license-name: GPL-2.0-or-later
type: antispam
author:
  - '[http://www.nexisonline.net/ Rob "N3X15" Nelson] <nexisentertainment@gmail.com>'
  - Aaron Schulz
url: http://www.mediawiki.org/wiki/Extension:ConfirmAccount
callback: ConfirmAccountStartup::efLoadConfirmAccount

AutoloadClasses:
  ConfirmAccountStartup: "ConfirmAccount.startup.php"
  # UI setup class
  ConfirmAccountUISetup: "frontend/ConfirmAccountUI.setup.php"
  # UI event handler classes
  ConfirmAccountUIHooks: "frontend/ConfirmAccountUI.hooks.php"

  # UI to request an account
  RequestAccountPage: "frontend/specialpages/actions/RequestAccount_body.php"
  # UI to confirm accounts
  ConfirmAccountsPage: "frontend/specialpages/actions/ConfirmAccount_body.php"
  ConfirmAccountsPager: "frontend/specialpages/actions/ConfirmAccountsPager.php"
  # UI to see account credentials
  UserCredentialsPage: "frontend/specialpages/actions/UserCredentials_body.php"

  # Utility functions
  ConfirmAccount: "backend/ConfirmAccount.class.php"
  # Data access objects
  UserAccountRequest: "backend/UserAccountRequest.php"

  # Business logic
  AccountRequestSubmission: "business/AccountRequestSubmission.php"
  AccountConfirmSubmission: "business/AccountConfirmSubmission.php"
  ConfirmAccountPreAuthenticationProvider: "business/ConfirmAccountPreAuthenticationProvider.php"

  ConfirmAccountUpdaterHooks: backend/schema/ConfirmAccountUpdater.hooks.php

MessagesDirs:
  ConfirmAccount:      i18n/core
  RequestAccountPage:  i18n/requestaccount
  ConfirmAccountPage:  i18n/confirmaccount
  UserCredentialsPage: i18n/usercredentials

SpecialPages:
  RequestAccount:  RequestAccountPage
  ConfirmAccounts: ConfirmAccountsPage
  UserCredentials: UserCredentialsPage

Hooks:
  # Make sure "login / create account" notice still as "create account"
  PersonalUrls: ConfirmAccountUIHooks::setRequestLoginLinks
  # Add notice of where to request an account at UserLogin
  UserCreateForm: ConfirmAccountUIHooks::addRequestLoginText
  UserLoginForm: ConfirmAccountUIHooks::addRequestLoginText
  # Status header like "new messages" bar
  BeforePageDisplay: ConfirmAccountUIHooks::confirmAccountsNotice
  # Register admin pages for AdminLinks extension.
  AdminLinks: ConfirmAccountUIHooks::confirmAccountAdminLinks
  # Pre-fill/lock the form if its for an approval
  AuthChangeFormFields: ConfirmAccountUIHooks::onAuthChangeFormFields

  LoadExtensionSchemaUpdates: ConfirmAccountUpdaterHooks::addSchemaUpdates

AuthManagerAutoConfig:
  preauth:
    ConfirmAccountPreAuthenticationProvider:
      class: ConfirmAccountPreAuthenticationProvider
      sort: 0

AvailableRights:
  # Let some users confirm account requests and view credentials for created accounts
  - confirmaccount # user can confirm account requests
  - requestips # user can see IPs in request queue
  - lookupcredentials # user can lookup info on confirmed users
  - confirmaccount-notify

GroupPermissions:
  # Restrict account creation
  '*':
    createaccount: no
  user:
    createaccount: no
  bureaucrat:
    confirmaccount: yes
    requestips: yes # This right has the request IP show when confirming accounts
    #confirmaccount-notify: yes # Receive emails when an account confirms its email address
    lookupcredentials: yes # If credentials are stored, this right lets users look them up

ResourceModules:
  ext.confirmAccount:
    styles: confirmaccount.css
    localBasePath: modules
    remoteExtPath: ConfirmAccount/frontend/modules

config:
  ###
  # Configuration for extra arguments that may be included in the admin email message.
  # Extra parameters that could be added:
  #  [ 'email', 'real_name', 'bio', 'notes', 'urls', 'ip' ]
  # The order you specify here is the order in which you can use the parameters.
  # $wgConfirmAdminEmailExtraFields is an array, the parameters 1 and 2 of the
  # message will always be: 1 - the username and 2 - the confirm url.
  ###
  ConfirmAdminEmailExtraFields:
    description: Configuration for extra arguments that may be included in the admin email message.
    value: []
  AccountRequestExts:
    description: If files can be attached, what types can be used? (MIME data is checked)
    value:
    - txt
    - pdf
    - doc
    - latex
    - rtf
    - text
    - wp
    - wpd
    - sxw
  AccountRequestThrottle:
    description: How many requests can an IP make at once?
    value: 1
  # Prospective account request types.
  # Format is an array of (integer => (subpage param,user group,autotext)) pairs.
  # The integer keys enumerate the request types. The key for a type should not change.
  # Each type has its own request queue at Special:ConfirmAccount/<subpage param>.
  # When a request of a certain type is approved, the new user:
  # (a) is placed in the <user group> group (if not User or *)
  # (b) has <autotext> appended to his or her user page
  AccountRequestTypes:
    description: Prospective account request types.
    value:
      0: ["authors", "user", null]
  AccountRequestWhileBlocked:
    description: Can blocked users with "prevent account creation" request accounts?
    value: false
  AutoUserBioText:
    description: Text to add to bio pages if the above option is on
    value: ''
  AutoWelcomeNewUsers:
    description: Create a user talk page with a welcome message for accepted users.
      The message can be customized by editing MediaWiki:confirmaccount-welc.
    value: true
  ConfirmAccountCaptchas:
    description: If ConfirmEdit is installed and set to trigger for createaccount,
      inject catpchas for requests too?
    value: true
  ConfirmAccountContact:
    description: Send an email to this address when account requestors confirm their
      email.
    value: false
  ConfirmAccountFSRepos:
    description: Storage repos.
    value:
      accountcreds:
        directory: false
        hashLevels: 3
        name: accountcreds
        url: null
      accountreqs:
        directory: false
        hashLevels: 3
        name: accountreqs
        url: null
  ConfirmAccountNotice:
    description: Show notice for open requests to admins? This is cached, but still
      can be expensive on sites with thousands of requests.
    value: true
  ConfirmAccountRejectAge:
    description: How long after accounts have been requested/held before they count
      as 'rejected'
    value: 2592000
  ConfirmAccountRequestFormItems:
    description: Which form elements to show at Special:RequestAccount
    value:
      AreasOfInterest:
        enabled: true
      Biography:
        enabled: true
        minWords: 6
      CV:
        enabled: true
      Links:
        enabled: true
      Notes:
        enabled: true
      RealName:
        enabled: true
      TermsOfService:
        enabled: true
      UserName:
        enabled: true
  ConfirmAccountSaveInfo:
    description: 'IMPORTANT: do we store the user''s notes and credentials for successful
      account request? This will be stored indefinetely and will be accessible to
      users with crediential lookup permissions'
    value: true
  ConfirmAccountSortkey:
    description: If set, will add {{DEFAULTSORT:sortkey}} to userpages for auto-categories.
    value: false
  MakeUserPageFromBio:
    description: Set the person's bio as their userpage?
    value: true
  RejectedAccountMaxAge:
    description: How long to store rejected requests
    value: 604800
