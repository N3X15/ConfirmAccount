# pip install jsonschema pyyaml
import json, jsonschema, yaml, os, sys

VALIDATOR=jsonschema.Draft3Validator
PADDING=15 # len(extension.json)+1
def validate(schema, instance):
    v = VALIDATOR(schema)
    if not v.is_valid(instance):
        print('FAILED TO VALIDATE')
        for err in v.iter_errors(instance):
            print('  /'+'/'.join(err.absolute_path)+': '+err.message)
        sys.exit(1)

def check_json_file(filename, schema=None):
    print(filename, end=' '*(PADDING - len(filename)))
    i = None
    with open(filename, 'r') as f:
        i = json.load(f)
    if schema:
        with open(schema, 'r') as f:
            s = json.load(f)
        validate(s,i)
    print('OK')

def check_yaml_file(filename, schema=None):
    print(filename, end=' '*(PADDING - len(filename)))
    i = None
    with open(filename, 'r') as f:
        i = yaml.load(f)
    if schema:
        with open(schema, 'r') as f:
            s = json.load(f)
        validate(s,i)
    print('OK')

check_json_file('extension.json', schema='dev/extension.schema.v2.json')
#check_yaml_file('mwi.yml')
